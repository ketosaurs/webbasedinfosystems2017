package nl.bioinf.wis1.servlets;

import nl.bioinf.wis1.dao.MyDataSourceDummy;
import nl.bioinf.wis1.dao.MyDataSourceException;
import nl.bioinf.wis1.dao.MyUserDatasource;
import nl.bioinf.wis1.users.Address;
import nl.bioinf.wis1.users.User;

import javax.security.auth.login.LoginException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "NewLoginServlet", urlPatterns = "/newLogin.do")
public class NewLoginServlet extends HttpServlet {
    private MyUserDatasource dataSource;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        HttpSession session = request.getSession();
        session.setMaxInactiveInterval(15);
        if (session.isNew() || session.getAttribute("user") == null) {
            try {
                User u = loginUser(username, password);
                session.setAttribute("user", u);
            } catch (LoginException e) {
                request.setAttribute("login_error", e.getMessage());
            }
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher("home.jsp");
        dispatcher.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("home.jsp");
        dispatcher.forward(request, response);
    }


    private User loginUser(String username, String password) throws LoginException {
        User user;
        try {
            user = dataSource.getUser(username, password);
        } catch (MyDataSourceException e) {
            throw new LoginException(e.getMessage());
        }
        return user;
    }

    @Override
    public void init() throws ServletException {
        super.init();
        String dbType = getServletContext().getInitParameter("db-type");

        dataSource = new MyDataSourceDummy();
        try {
            dataSource.connect();
        } catch (MyDataSourceException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void destroy() {
        super.destroy();
        try {
            dataSource.disconnect();
        } catch (MyDataSourceException e) {
            e.printStackTrace();
        }
    }
}
