package nl.bioinf.wis1.servlets;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "LoginServlet", urlPatterns = "/login.do")
public class LoginServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        HttpSession session = request.getSession();
//        session.setMaxInactiveInterval(1000);
        String dispatchPage = "SuperSecret.jsp";
        if (session.isNew() || session.getAttribute("user") == null) {
            boolean success = loginUser(username, password);
            if (success) {
                session.setAttribute("user", username);
            } else {
                request.setAttribute("login_error", "I do not know you! Please get out.");
                dispatchPage = "Login.jsp";

            }
        }
        RequestDispatcher dispatcher = request.getRequestDispatcher(dispatchPage);
        dispatcher.forward(request, response);
    }

    private boolean loginUser(String username, String password) {
        return (username.equalsIgnoreCase("Henk")
                && password.equalsIgnoreCase("Henk"));
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher("Login.jsp");
        dispatcher.forward(request, response);
    }
}
