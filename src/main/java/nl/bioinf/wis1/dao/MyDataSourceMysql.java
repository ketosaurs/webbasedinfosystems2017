package nl.bioinf.wis1.dao;

import nl.bioinf.noback.db_utils.DbCredentials;
import nl.bioinf.noback.db_utils.DbUser;
import nl.bioinf.wis1.users.User;

import java.io.IOException;
import java.sql.*;

public class MyDataSourceMysql implements MyUserDatasource {
    private final String host;
    private final String dbUser;
    private final String dbPassword;
    private final String dbName;
    private Connection connection;
    private PreparedStatement insertUserPreparedStatement;
    private PreparedStatement getUserPreparedStatement;

    public MyDataSourceMysql(String host, String dbUser, String dbPassword, String dbName) {
        this.host = host;
        this.dbUser = dbUser;
        this.dbPassword = dbPassword;
        this.dbName = dbName;
    }

    @Override
    public void connect() throws MyDataSourceException {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String dbUrl = "jdbc:mysql://" + this.host + "/" + this.dbName;
            connection = DriverManager.getConnection(dbUrl, this.dbUser, this.dbPassword);
            prepareStatements();
            System.out.println("connected");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private void prepareStatements() throws SQLException {
        String insertQuery = "INSERT INTO Users (user_name, user_password, user_email) "
                + " VALUES (?, ?, ?)";
        this.insertUserPreparedStatement = connection.prepareStatement(insertQuery);

        String getUserQuery = "SELECT * FROM Users WHERE user_name = ? AND user_password = ?;";
        this.getUserPreparedStatement = connection.prepareStatement(getUserQuery);
    }

    @Override
    public void disconnect() throws MyDataSourceException {
        try {
            connection.close();
            insertUserPreparedStatement.close();
            getUserPreparedStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public User getUser(String userName, String userPassword) throws MyDataSourceException {
        try {
            getUserPreparedStatement.setString(1, userName);
            getUserPreparedStatement.setString(2, userPassword);
            ResultSet resultSet = getUserPreparedStatement.executeQuery();
            if (resultSet.next()) {
                String userMail = resultSet.getString("user_email");
                String userpass = resultSet.getString("user_password");
                String username = resultSet.getString("user_name");

                System.out.println("userMail = " + userMail);
                System.out.println("userIdStr = " + userpass);
                System.out.println("username = " + username);
            } else {

            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void insertUser(User newUser) throws MyDataSourceException {
        try {
            insertUserPreparedStatement.setString(1, newUser.getUserName());
            insertUserPreparedStatement.setString(2, newUser.getPassWord());
            insertUserPreparedStatement.setString(3, newUser.getEmail());
            insertUserPreparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        try {
            DbUser dbUser = DbCredentials.getMySQLuser();
            System.out.println("dbUser.getDatabasename() = " + dbUser.getDatabaseName());
            System.out.println("dbUser.getHost() = " + dbUser.getHost());
            System.out.println("dbUser.getUserName() = " + dbUser.getUserName());

            MyDataSourceMysql db = new MyDataSourceMysql(dbUser.getHost(),
                                                        dbUser.getUserName(),
                                                        dbUser.getDatabasePassword(),
                                                        dbUser.getDatabaseName());
            db.connect();
            User u = new User("Moos", "Moos");
            u.setEmail("Moos@example.com");
            db.insertUser(u);

            db.getUser("Koos", "Koos");
            
            db.disconnect();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (MyDataSourceException e) {
            e.printStackTrace();
        }

    }

}
